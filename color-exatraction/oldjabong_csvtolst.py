basedir = "/mnt/lire500/refimages/s3imgs/vendors/"
vendor = "jabong"
import csv
import os
import json
json_map_file = json.load(open('category_mapping_abof.json', 'r'));
cat_map = {"Women:Tops:Apparel & Accessories > Clothing > Shirts & Tops": "Women:Clothing:Top Wear:Tops:", "Women:Pendants & Pendant Sets:Apparel & Accessories > Jewelry": "Women:Jewellery:Fashion Jewellery:Pendants & Pendant Sets:", "Women:Trousers:Leggings & others": "Women:Clothing:Leggings & Jeggings:Leggings & Jeggings:", "Men:Formal Trousers:Apparel & Accessories > Clothing > Pants": "Men:Clothing:Trousers & Chinos:Men Trousers:", "Women:Twin Sets & Jumpsuits:Apparel & Accessories > Clothing": "Women:Clothing:Dresses & Jumpsuits:Dresses & Jumpsuits:", "Women:Track Wear:Apparel & Accessories > Clothing > Activewear": "Women:Clothing:Track Wear:Women Trackwear:", "Women:Trackpants:Apparel & Accessories > Clothing": "Women:Clothing:Track Wear:Women Trackwear:", "Women:Necklaces & Necklace Sets:Apparel & Accessories > Jewelry > Necklaces": "Women:Jewellery:Fashion Jewellery:Necklaces & Necklace Sets:", "Men:Suits & Blazers:Apparel & Accessories > Clothing > Outerwear > Coats & Jackets": "Men:Clothing:Suits & Blazers:blazersuits:", "Women:Tops:Tees & Shirts": "Women:Clothing:Top Wear:Tops:", "Women:Trousers:Apparel & Accessories > Clothing": "Women:Clothing:Trousers & Jeans:Women Trouser:", "Women:Tees:Apparel & Accessories > Clothing > Shirts & Tops > T-Shirts": "Women:Clothing:Top Wear:Women Tees:", "Women:Travel Bags:Luggage & Bags>Suitcases": "Women:Bags:Wallet, Purses & Potlis:Wallet:", "Women:Wedges:Apparel & Accessories > Shoes": "Women:Shoes:Heels:Heels:", "Women:Shorts & Capris:Apparel & Accessories > Clothing": "Women:Clothing:Capris, Shorts & Skirts:Women Shorts:", "Men:Flip Flops:Apparel & Accessories>Shoes": "Men:Shoes:Flip Flops:Flip Flops:", "Men:Casual Trousers:Apparel & Accessories > Clothing > Pants": "Men:Clothing:Trousers & Chinos:Men Chino Pants:", "Men:Ethnic & Party Wear:Apparel & Accessories > Clothing": "Men:Clothing:Kurtas, Pyjamas & Sherwanis:Men Ethnic Top:", "Women:Kurtas & Kurtis:Apparel & Accessories > Clothing": "Women:Clothing:Top Wear:Kurta:", "Women:Winter Jackets:Apparel & Accessories > Clothing": "Women:Clothing:Winter Wear:Women Jackets:", "Men:Winter Wear:Apparel & Accessories > Clothing": "Men:Clothing:Jackets:Men outerwear:", "Men:Trackwear & Nightwear:Apparel & Accessories > Clothing": "Men:Clothing:Trackpants & Tracksuits:Men Track Pants:", "Men:Pendants & Lockets:Apparel & Accessories > Jewelry": "Men:Jewellery:Fashion Jewellery:Necklace:", "Men:Windcheaters:Apparel & Accessories > Clothing > Activewear > Active Jackets > Windbreakers": "Men:Clothing:Jackets:Men outerwear:", "Women:Capris:Shorts & Skirts": "Women:Clothing:Capris, Shorts & Skirts:Capris:", "Women:Rings & Bands:Apparel & Accessories > Jewelry": "Women:Jewellery:Fashion Jewellery:Rings & Bands:", "Women:Belly Shoes:Apparel & Accessories > Shoes": "Women:Shoes:Flats:Flats:", "Women:Stilettos:Apparel & Accessories > Shoes": "Women:Shoes:Heels:Heels:", "Women:Dungarees & Jumpsuits:Apparel & Accessories > Clothing": "Women:Clothing:Dresses & Jumpsuits:Dresses & Jumpsuits:", "Men:Denim Shirts:Apparel & Accessories > Clothing": "Men:Clothing:Shirts:Casual Shirts:", "Women:Bottoms:Apparel & Accessories>Clothing": "Women:Clothing:Trousers & Jeans:Women Trouser:", "Women:Earrings:Apparel & Accessories > Jewelry > Earrings": "Women:Jewellery:Fashion Jewellery:Ear Studs:", "Women:Shopping Bags & Totes:Luggage & Bags>Shopping Totes": "Women:Bags:Wallet, Purses & Potlis:Wallet:", "Women:Mangalsutras:Apparel & Accessories > Jewelry": "Women:Jewellery:Fashion Jewellery:Traditional Jewellery:Mangalsutra and Earrings", "Women:Traditional Jewellery:Apparel & Accessories > Jewelry": "Women:Jewellery:Fashion Jewellery:Traditional Jewellery:", "Women:Nosepins:Apparel & Accessories > Jewelry": "Women:Jewellery:Precious Jewellery:Nosepin:", "Men:Jeans:Apparel & Accessories > Clothing > Pants > Jeans": "Men:Clothing:Jeans:Men Jeans:", "Men:Sports Shorts:Apparel & Accessories>Clothing": "Men:Clothing:Shorts & 3/4ths:Men Shorts:", "Women:Wallet:Purses & Potlis": "Women:Bags:Wallet, Purses & Potlis:Wallet:", "Women:Suit Set:Apparel & Accessories > Clothing": "Women:Clothing:Kurtas & Suit Set:Kurta:", "Women:Dresses:Apparel & Accessories > Clothing": "Women:Clothing:Dresses & Jumpsuits:Dresses & Jumpsuits:", "Men:Casual Shoes:Apparel & Accessories > Shoes": "Men:Shoes:Casual Shoes:Casual Shoes:", "Men:Denim Jackets:Apparel & Accessories > Clothing": "Men:Clothing:Jackets:Men outerwear:", "Women:Flats:Apparel & Accessories > Shoes > Flats": "Women:Shoes:Flats:Flats:", "Women:Bottoms & Dupatta Sets:Apparel & Accessories > Clothing": "Women:Clothing:Salwars & Churidars:Salwars & Churidars:", "Women:Tunics:Apparel & Accessories > Clothing": "Women:Clothing:Top Wear:Tops:", "Women:Clutches:Apparel & Accessories>Handbags": "Women:Bags:Wallet, Purses & Potlis:Wallet:", "Women:Bangles & Bracelets:Apparel & Accessories > Jewelry > Bracelets": "Women:Jewellery:Fashion Jewellery:Bangles:", "Women:Shirts:Apparel & Accessories > Clothing > Shirts & Tops": "Women:Clothing:Top Wear:Shirts:", "Women:Watches:Apparel & Accessories>Jewelry>Watches": "Women:Accessories:Watches:Watche:", "Men:Sweatshirts & Hoodies:Apparel & Accessories > Clothing > Shirts & Tops > Sweaters & Cardigans": "Men:Clothing:Sweatshirts:Sweatshirts:", "Women:Sleepwear:Apparel & Accessories > Clothing > Sleepwear & Loungewear": "Women:Clothing:Lingerie & Sleepwear:Nightwears:", "Women:Suit Sets:Apparel & Accessories > Clothing": "Women:Clothing:Kurtas & Suit Set:Kurta:", "Women:Bellies:Apparel & Accessories>Shoes": "Women:Shoes:Flats:Flats:", "Women:Salwars & Churidars:Apparel & Accessories > Clothing": "Women:Clothing:Salwars & Churidars:Salwars & Churidars:", "Men:T-Shirts & Polos:Apparel & Accessories > Clothing": "Men:Clothing:T-Shirts & Polos:Men Tees:", "Women:Sarees:Apparel & Accessories > Clothing": "Women:Clothing:Sarees & Dress Material:Sarees:", "Men:Loafers:Apparel & Accessories>Shoes>Loafers & Slip-Ons": "Men:Shoes:Casual Shoes:Casual Shoes:", "Men:Ties & Cufflinks:Apparel & Accessories > Clothing Accessories > Cufflinks": "Men:Clothing:Clothing Accessories:Tie:", "Women:Heels:Apparel & Accessories > Shoes": "Women:Shoes:Heels:Heels:", "Women:Shrugs:Apparel & Accessories > Clothing": "Women:Clothing:Winter Wear:Women winterwear:", "Women:Summer Jackets:Apparel & Accessories > Clothing": "Women:Clothing:Winter Wear:Women Jackets:", "Men:Slippers:Apparel & Accessories>Shoes": "Men:Shoes:Flip Flops:Flip Flops:", "Women:Ethnic Jackets:Apparel & Accessories > Clothing": "Women:Clothing:Kurtas & Suit Set:Kurta:", "Women:Anklets & Toe Rings:Apparel & Accessories > Jewelry": "Women:Jewellery:Fashion Jewellery:Anklets & Toe Rings:", "Men:Socks:Apparel & Accessories > Clothing > Underwear & Socks > Socks": "Men:Clothing:Clothing Accessories:Socks:", "Women:Peep Toes:Apparel & Accessories > Shoes": "Women:Shoes:Flats:Flats:", "Men:Sweaters:Apparel & Accessories > Clothing > Shirts & Tops > Sweaters & Cardigans": "Men:Clothing:Sweaters:Sweaters:", "Men:Clothing Accessories & Innerwear:Apparel & Accessories > Clothing > Baby & Toddler Clothing": "Men:Clothing:Innerwear & Sleepwear:Innerwear & Sleepwear:", "Men:Formal Shirts:Apparel & Accessories > Clothing > Activewear > Active Shirts": "Men:Clothing:Shirts:Formal Shirts:", "Women:Tops & T-shirts:Apparel & Accessories > Clothing": "Women:Clothing:Top Wear:Women Tees:", "Women:Brooches:Apparel & Accessories > Jewelry": "Women:Jewellery:Fashion Jewellery:Brooches:", "Women:Hand Bags:Apparel & Accessories>Handbags": "Women:Bags:Wallet, Purses & Potlis:Wallet:", "Men:Sports:Sporting Goods>Team Sports": "Men:Accessories:Sports Wear:Sports Wear:", "Women:Jeans & Trousers:Apparel & Accessories > Clothing": "Women:Clothing:Trousers & Jeans:Women Jeans:", "Women:Sling Bags:Luggage & Bags>Duffel Bags>": "Women:Bags:Wallet, Purses & Potlis:Wallet:", "Women:Loungewear:Apparel & Accessories > Clothing > Sleepwear & Loungewear": "Women:Clothing:Lingerie & Nightwear:Nightwear:Lounge Set", "Men:Ethnic Wear:Apparel & Accessories > Clothing > Traditional & Ceremonial Clothing": "Men:Clothing:Kurtas, Pyjamas & Sherwanis:Men Ethnic Top:", "Women:Sports Shoes:Apparel & Accessories>Shoes": "Women:Shoes:Sports Shoes:Sports Shoes:", "Women:Sweatshirts:Apparel & Accessories > Clothing > Shirts & Tops > Sweaters & Cardigans": "Women:Clothing:Women winterwear:Sweatshirts:", "Men:Shorts & 3/4ths:Apparel & Accessories > Clothing > Activewear > Active Shorts": "Men:Clothing:Shorts & 3/4ths:Men Shorts:", "Men:Wallets:Apparel & Accessories > Handbags": "Men:Bags:Wallets:Wallets:", "Men:Formal Shoes:Apparel & Accessories>Shoes": "Men:Shoes:Formal Shoes:Formal Shoes:", "Women:Lehengas:Apparel & Accessories > Clothing": "Women:Clothing:Sarees & Dress Material:Lehengas:", "Men:Polos & Tees:Apparel & Accessories > Clothing > Shirts & Tops > T-Shirts": "Men:Clothing:T-Shirts & Polos:Men Polo Tees:", "Women:Peeptoes:Apparel & Accessories > Shoes": "Women:Shoes:Flats:Flats:", "Men:Casual Shirts:Apparel & Accessories > Clothing > Shirts & Tops > T-Shirts": "Men:Clothing:Shirts:Casual Shirts:", "Men:Jackets:Apparel & Accessories>Clothing": "Men:Clothing:Jackets:Men outerwear:"}

def do_need(line):
	print line[0].strip()
def createDir(path):
        os.system('mkdir -p '+path)

def modifyName(name):
        name = name.replace(' ','_')
        name = name.replace('&','_')
        name = name.replace('/','_')
        name = name.replace(',','_')
        name = name.replace('|','_')
        name = name.replace('-','_')
        return name
def main():
        with open('csvfiles/jabong_feeds.csv') as csvfile:
		csvreader = csv.reader(csvfile)
            	next(csvfile)
		for line in csvreader:
			id = line[0].strip()
                        title = line[2].strip()
                        image = line[1]
                        description = line[3]
                        main_cat = line[5].strip()
                        sub_category = line[6].strip()
                        category = line[7].strip()
                        link = line[4]
                        if main_cat != 'Men':
                                continue

                        if main_cat + ':' + sub_category + ':' + category not in cat_map:
                                continue
                        master_cat = cat_map[main_cat + ':' + sub_category + ':' + category]
                        main_cat = master_cat.split(':')[0]
                        sub_cat = master_cat.split(':')[2]
                        subsub_cat = master_cat.split(':')[3]
                        type = master_cat.split(':')[4]
                        sub_cat = modifyName(sub_cat)
                        subsub_cat = modifyName(subsub_cat)
                        type = modifyName(type)
			imgname = id+'_372x496.jpg'	
			mapcat =sub_cat+"_"+subsub_cat
			gender = main_cat.lower();
			mapcat = mapcat.replace("__","_")
			try:
				category_lst = json_map_file[gender][mapcat]
			except:
				continue
				
			if type == "":
				local_path = "/mnt/lire500/refimages/s3imgs/jabong_grabcut/"+main_cat+"/"+sub_cat+"/"+subsub_cat+"/"+imgname
			else:
				local_path = "/mnt/lire500/refimages/s3imgs/jabong_grabcut/"+main_cat+"/"+sub_cat+"/"+subsub_cat+"/"+type+"/"+imgnamee
			
			final_line = link+"||"+title+"||"+category_lst+"||"+local_path+"||"+vendor+"||"+image
			
			path = basedir+vendor+"/oldlst/"+gender+"/"
			if not os.path.isdir(path):
				createDir(path)
			
			with open(path+category_lst+'.lst', 'a') as the_file:
				the_file.write(final_line+'\n')
							
if __name__ == '__main__':
        main()

