var ColorThief = require('color-thief');
var rgbHex = require('rgb-hex');
var fs = require('fs')
var namer = require('color-namer')
var colorThief = new ColorThief();
var vendor = "jabong";
var gender = "women";
var basedir = "/mnt/lire500/refimages/s3imgs/vendors/"+vendor+"/oldct-lst/"+gender+"/";
var logdir = "/mnt/lire500/refimages/s3imgs/vendors/"+vendor+"/oldlogs/"+gender+"/";
var filename = "/mnt/lire500/refimages/s3imgs/vendors/"+vendor+"/oldlst/"+gender+"/"+process.argv[2];
var sourceImage = "/mnt/lire500/refimages/s3imgs/myntra_grabcut2/Men/Sweatshirts/Sweatshirts/Voi-Jeans-Men-Sweatshirts_2a699b2b68facad473f2e03c9210a045_images_372x496.jpg"
var minlen = 5000;
fs.readFile(filename, 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  var array = data.toString().split("\n");
var catlength = array.length;
/*var iteration = Math.ceil(catlength/minlen);
console.log(iteration);
if(catlength > minlen){
var end = minlen;
}else{
var end = catlength;
}*/
for(k=0;k<array.length;k++) {
        var pgurl = array[k].split("||")[0]; 
	var title = array[k].split("||")[1];
	var category = array[k].split("||")[2];
	var s_image = array[k].split("||")[3];
	var vendor = array[k].split("||")[4];
	
	/*
	var pgurl = array[k].split("||")[2]; 
        var title = array[k].split("||")[1];
        var category = array[k].split("||")[3];
        var s_image = array[k].split("||")[0];
        console.log(s_image);
	var vendor = "myntra";*/
	
if (fs.existsSync(s_image)) {
try{
colinfo_arr = colorThief.getPalette(s_image);
}catch(err){
continue;
}
var colsum = calcsum(colinfo_arr);
colinfo_arr = colinfo_arr.sort(sortFunction);
var hex_arr = [];
var name_arr = [];
var per_arr = [];
for(i=0;i<colinfo_arr.length;i++){
var color_val = colinfo_arr[i];
var r  = color_val[0];
var g = color_val[1];
var b = color_val[2];
var per = (color_val[3]/colsum)*100;
per_arr.push(per);
var hexval = rgbHex(r,g,b);
hex_arr.push(hexval);
var names = namer("#"+hexval);
var name = names.pantone[0].name;
name_arr.push(name);
}
var final_val = pgurl+"||"+title+"||"+category+"||"+s_image+"||"+vendor+"||"+name_arr[0]+"||"+name_arr[1]+"||"+name_arr[2]+"||"+name_arr[3]+"||"+name_arr[4]+"||"+name_arr[5]+"||"+name_arr[6]+"||"+name_arr[7]+"||"+name_arr[8]+"||"+per_arr[0]+"||"+per_arr[1]+"||"+per_arr[2]+"||"+per_arr[3]+"||"+per_arr[4]+"||"+per_arr[5]+"||"+per_arr[6]+"||"+per_arr[7]+"||"+per_arr[8]+"||"+hex_arr[0]+"||"+hex_arr[1]+"||"+hex_arr[2]+"||"+hex_arr[3]+"||"+hex_arr[4]+"||"+hex_arr[5]+"||"+hex_arr[6]+"||"+hex_arr[7]+"||"+hex_arr[8];
console.log(k+"/"+catlength+" for "+category);
//console.log(final_val);
fs.appendFileSync(logdir+category+".lst",k+"/"+catlength+" for "+category+"\n");
fs.appendFileSync(basedir+category+".lst", final_val+"\n");
}
else{
console.log(k+"/"+catlength+" for "+category+" : file not found");
fs.appendFileSync(logdir+category+".lst",k+"/"+catlength+" for "+category+" file:"+s_image+" file did not exist  \n");
}
}
});


function calcsum(arr){
var sum = 0;
for(j=0;j<arr.length;j++){
sum =sum+arr[j][3];
}
return sum

}
function sortFunction(a, b) {
    if (a[3] === b[3]) {
        return 0;
    }
    else {
        return (a[3] > b[3]) ? -1 : 1;
    }
}
